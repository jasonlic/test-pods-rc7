

Pod::spec.new do |s|


  s.name         = "TestPodsRC7"
  s.version      = '0.0.3'
  s.summary      = "test pods"

  s.description  = <<-DESC
	test pods
                   DESC
  s.homepage     = "http://apple.com.cn"

  s.license      = 'Apache License, Version 2.0'

  s.author             = { "RC" => "apple@apple.com" }

  s.platform     = :ios, "11.0"


  s.source       = { :git => "https://gitee.com/jasonlic/test-pods-rc7.git", :tag => s.version.to_s }
  s.source_files  = "RoomUI", "RoomUI/**/*"
  s.dependency 'Masonry'
  s.dependency 'Bugly'
  s.dependency 'SDWebImage', '5.14.2'
  s.dependency 'MBProgressHUD'
  s.dependency 'MJExtension'
  s.dependency 'MJRefresh'
  s.dependency 'YYWebImage'
  s.dependency 'YYCache'
  s.dependency 'YYImage'
  s.dependency 'YYText'
  s.dependency 'SnapKit'
  s.dependency 'AliyunOSSiOS'

  # 基础SDK
  s.dependency 'CCLivePlaySDK', '4.12.0'
  s.dependency 'HDVideoClass_BSDK', '6.25.0'
  s.dependency 'AgoraRtcEngine_iOS', '3.7.2'
  s.dependency 'HDSAliyunPlayer'
  s.dependency 'HDStreamLib'
  s.dependency 'HDSCocoaLumberjack'
  s.dependency 'HDSSZip'

  s.dependency 'WFPopupManager'

  # 互动功能
  s.dependency 'HDSInteractionEngine',  '4.9.0'
  s.dependency 'HDSLikeModule',  '4.9.0'
  s.dependency 'HDSGiftModule',  '4.9.0'
  s.dependency 'HDSVoteModule',  '4.9.0'
  s.dependency 'HDSRedEnvelopeModule',  '4.9.0'
  s.dependency 'HDSInvitationCardModule',  '4.9.0'
  s.dependency 'HDSQuestionnaireModule',  '4.9.0'
  s.dependency 'HDSLiveStoreModule',  '4.9.0'


  # VR
  s.dependency 'HDSPanoramicVideoFramework'
  s.dependency 'HDSVRMediaFramework'

  s.dependency 'LookinServer', :configurations => ['Debug']

end
